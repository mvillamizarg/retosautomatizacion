package co.com.proyectobase.screenplay.userinterface;

import org.openqa.selenium.By;

import net.serenitybdd.screenplay.targets.Target;

public class AdministrarCitaPageObject {
	
	public static final Target MENU_CITA = Target.the("Menú de Citas").located(By.xpath("//*[@id=\'page-wrapper\']/div/div[2]/div/div/div/div/div[1]/div/a[6]"));
	public static final Target FECHA_CITA = Target.the("Campo para ingresar fecha de la cita").located(By.id("datepicker"));
	public static final Target DOCUMENTO_PACIENTE = Target.the("Campo para ingresar número documento del paciente").located(By.xpath("//*[@id=\'page-wrapper\']/div/div[3]/div/div[2]/input"));
	public static final Target DOCUMENTO_DOCTOR = Target.the("Campo para ingresar número documento del doctor").located(By.xpath("//*[@id=\'page-wrapper\']/div/div[3]/div/div[3]/input"));
	public static final Target OBSERVACION = Target.the("Campo para ingresar fecha de la cita").located(By.xpath("//*[@id=\'page-wrapper\']/div/div[3]/div/div[4]/textarea"));
	public static final Target BOTON = Target.the("Campo para ingresar fecha de la cita").located(By.xpath("//*[@id=\'page-wrapper\']/div/div[3]/div/a"));
}
