package com.choucair.formacion.pageobjects;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class ColorlibFormValidationPage extends PageObject {
	
	// Campo Required
	@FindBy(xpath="//*[@id=\'req\']")
	public WebElementFacade txtRequired;
	
	//Campo Seleccción deporte
	@FindBy(xpath="//*[@id=\'sport\']")
	public WebElementFacade cmbSport;
	
	//Campo Multiple Selección deporte
	@FindBy(xpath="//*[@id=\'sport2\']")
	public WebElementFacade cmbSport2;
	
	//Campo Url
	@FindBy(xpath="//*[@id=\'url1\']")
	public WebElementFacade txtUrl;
	
	//Campo Email
	@FindBy(xpath="//*[@id=\'email1\']")
	public WebElementFacade txtEmail;
	
	//Campo Password1
	@FindBy(xpath="//*[@id=\'pass1\']")
	public WebElementFacade txtPass1;
	

	//Campo Password2
	@FindBy(xpath="//*[@id=\'pass2\']")
	public WebElementFacade txtPass2;
	
	//Campo MinSize
	@FindBy(xpath="//*[@id=\'minsize1\']")
	public WebElementFacade txtMinSize;
	
	//Campo MaxSize
	@FindBy(xpath="//*[@id=\'maxsize1\']")
	public WebElementFacade txtMaxSize;
	
	//Campo Number
	@FindBy(xpath="//*[@id=\'number2\']")
	public WebElementFacade txtNumber;
	
	//Campo IP
	@FindBy(xpath="//*[@id=\'ip\']")
	public WebElementFacade txtIp;
	
	//Campo Date
	@FindBy(xpath="//*[@id=\'date3\']")
	public WebElementFacade txtDate;
	
	//Campo Date Earlier
	@FindBy(xpath="//*[@id=\'past\']")
	public WebElementFacade txtDateEarlier;
	
	//Boton Validate
	@FindBy(xpath="//*[@id='popup-validation']/div[14]/input")
	public WebElementFacade btnValidate;
	
	//Globo Informativo
	@FindBy(xpath="//DIV[@class='formErrorContent']")
	public WebElementFacade globoInformativo;
	
	//Metodos
	
	public void required(String datoPrueba){
		txtRequired.click();
		txtRequired.clear();
		txtRequired.sendKeys(datoPrueba);
	}
	
	public void select_Sport(String datoPrueba){
		cmbSport.click();
		cmbSport.selectByVisibleText(datoPrueba);
	}
	
	public void multiple_Select(String datoPrueba){
		cmbSport2.selectByVisibleText(datoPrueba);
	}
	
	public void url(String datoPrueba){
		txtUrl.click();
		txtUrl.clear();
		txtUrl.sendKeys(datoPrueba);
	}
	
	public void email(String datoPrueba){
		txtEmail.click();
		txtEmail.clear();
		txtEmail.sendKeys(datoPrueba);
	}
	
	public void password(String datoPrueba){
		txtPass1.click();
		txtPass1.clear();
		txtPass1.sendKeys(datoPrueba);
	}
	
	public void confirm_Password(String datoPrueba){
		txtPass2.click();
		txtPass2.clear();
		txtPass2.sendKeys(datoPrueba);
	}
	
	public void minimum_field_size(String datoPrueba){
		txtMinSize.click();
		txtMinSize.clear();
		txtMinSize.sendKeys(datoPrueba);
	}
	
	public void maximum_field_size(String datoPrueba){
		txtMaxSize.click();
		txtMaxSize.clear();
		txtMaxSize.sendKeys(datoPrueba);
		
	}
	
	public void number(String datoPrueba){
		txtNumber.click();
		txtNumber.clear();
		txtNumber.sendKeys(datoPrueba);
	}
	
	public void ip(String datoPrueba){
		txtIp.click();
		txtIp.clear();
		txtIp.sendKeys(datoPrueba);
	}
	
	public void date (String datoPrueba){
		txtDate.click();
		txtDate.clear();
		txtDate.sendKeys(datoPrueba);
	}
	
	public void date_Earlier(String datoPrueba){
		txtDateEarlier.click();
		txtDateEarlier.clear();
		txtDateEarlier.sendKeys(datoPrueba);
	}
	
	public void Validate(){
		btnValidate.click();	
	}
	
	public void form_sin_errores(){
		assertThat(globoInformativo.isCurrentlyVisible(), is(false));
		
	}
	public void form_con_errores(){
		assertThat(globoInformativo.isCurrentlyVisible(), is(true));
}
	
}
