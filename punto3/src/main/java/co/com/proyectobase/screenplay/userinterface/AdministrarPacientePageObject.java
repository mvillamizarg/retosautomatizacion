package co.com.proyectobase.screenplay.userinterface;


import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;


public class AdministrarPacientePageObject extends PageObject {

	public static final Target MENU_PACIENTE = Target.the("Menu Paciente").located(By.xpath("//*[@id=\'page-wrapper\']/div/div[2]/div/div/div/div/div[1]/div/a[2]"));
	public static final Target NOMBRE_PACIENTE = Target.the("Campo para ingresar el nombre").located(By.name("name"));
	public static final Target APELLIDO_PACIENTE = Target.the("campo para ingresar el apellido").located(By.name("last_name"));
	public static final Target TELEFONO_PACIENTE = Target.the("Campo para ingresar el telefono").located(By.name("telephone"));
	public static final Target TIPO_DOCUMENTO = Target.the("Campo para seleccionar tipo documento").located(By.name("identification_type"));
	public static final Target DOCUMENTO = Target.the("Campo para ingresar el numero de indentidad").located(By.name("identification"));
	public static final Target CHECK_PREPAGADA = Target.the("Check prepagada").located(By.name("prepaid"));
	public static final Target BOTON_GUARDAR = Target.the("Botón almacenar datos paciente").located(By.xpath("//*[@id=\'page-wrapper\']/div/div[3]/div/a"));

}