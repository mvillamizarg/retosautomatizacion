package com.choucair.formacion.steps;

import java.util.List;

import com.choucair.formacion.pageobjects.ColorlibFormValidationPage;

import net.thucydides.core.annotations.Step;

public class ColorlibFormValidationSteps {
	
	ColorlibFormValidationPage colorlibFormValidationPage;

	//Diligenciar Formulario Popup Validation
	@Step
	public void diligenciar_popup_datos_tabla(List<List<String>> data, int i) {
		colorlibFormValidationPage.required(data.get(i).get(0).trim());
		colorlibFormValidationPage.select_Sport(data.get(i).get(1).trim());
		colorlibFormValidationPage.multiple_Select(data.get(i).get(2).trim());
		colorlibFormValidationPage.multiple_Select(data.get(i).get(3).trim());
		colorlibFormValidationPage.url(data.get(i).get(4).trim());
		colorlibFormValidationPage.email(data.get(i).get(5).trim());
		colorlibFormValidationPage.password(data.get(i).get(6).trim());
		colorlibFormValidationPage.confirm_Password(data.get(i).get(7).trim());
		colorlibFormValidationPage.minimum_field_size(data.get(i).get(8).trim());
		colorlibFormValidationPage.maximum_field_size(data.get(i).get(9).trim());
		colorlibFormValidationPage.number(data.get(i).get(10).trim());
		colorlibFormValidationPage.ip(data.get(i).get(11).trim());
		colorlibFormValidationPage.date(data.get(i).get(12).trim());
		colorlibFormValidationPage.date_Earlier(data.get(i).get(13).trim());
		colorlibFormValidationPage.Validate();
	}

	//Verifico ingreso 
	@Step
	public void verificar_ingreso_formulario_exitoso(){
		colorlibFormValidationPage.form_sin_errores();
		
	}
	
	@Step
	public void verificar_ingreso_formulario_con_errores(){
		colorlibFormValidationPage.form_con_errores();
	}
	}
