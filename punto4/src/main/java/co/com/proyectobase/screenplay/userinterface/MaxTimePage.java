package co.com.proyectobase.screenplay.userinterface;

import org.openqa.selenium.By;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("http://www.choucairtesting.com:18000/MaxTimeCHC/Login.aspx")
public class MaxTimePage extends PageObject {

	public static final Target USUARIO = Target.the("Campo para ingresar el Login").located(By.id("Logon_v0_MainLayoutEdit_xaf_l30_xaf_dviUserName_Edit_I")) ; 
	public static final Target PASSWORD = Target.the("Campo para ingresar el password").located(By.id("Logon_v0_MainLayoutEdit_xaf_l35_xaf_dviPassword_Edit_I"));
	public static final Target BOTON = Target.the("Botón de acceso").located(By.id("Logon_PopupActions_Menu_DXI0_T"));
	public static final Target TABLA = Target.the("Columna del analista").located(By.xpath("//*[@id=\'Vertical_v1_LE_v2_DXHeadersRow0\']/td[4]/table/tbody"));
	public static final Target DESPLEGABLE = Target.the("Imagen de la lista desplegable").located(By.xpath("//*[@id=\'Vertical_v1_LE_v2_col1\']/table/tbody/tr/td[2]/img[2]"));
	public static final Target LISTA_DESPLEGABLE = Target.the("Lista desplegable").located(By.id("Vertical_v1_LE_v2_HFListBox_LBT"));
	public static final Target DIA = Target.the("Día que se va a selecionar").located(By.xpath("//*[@id=\'Vertical_v1_LE_v2_cell0_0_xaf_Fecha\']/tbody"));
	public static final Target MENSAJE = Target.the("Mensaje de ingreso").located(By.id("Vertical_VCC_VSL"));

}
