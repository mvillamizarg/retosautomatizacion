#Author: your.email@your.domain.com
#language:es
@Regresion
Característica: Reportar mis actividades en el sistema de Registro 
  	Como analista de pruebas
		Quiero realizar el reporte de mis actividades en MaxTime
		Para cumplir con la tarea que debo llevar a cabo diariamente.
	
	@CasoExitoso
	Escenario: Reportar tiempo MaxTime
		Dado que Mayra quiere reportar sus actividades en MaxTime con usuario "mvillamizarg" y contraseña "!=Elbaby1508"
		Cuando ella realiza el registro de la tareas en la aplicación
		|proyecto|tipoHora|servicio|actividad|causalOcioso|horasEjecutadas|horasAdicionalesP|horasAdicionalesC|comentario|
		|GBCO229 |H. Proyecto|P. generales CVDS ágiles|CAPACITACIÓN RECIBIDA EN PROCESO DE NEGOCIO DEL CLIENTE - FACTURABLE||4|||Prueba1|
		|GBCO229 |H. Proyecto|P. generales CVDS ágiles|CAPACITACIÓN RECIBIDA EN PROCESO DE NEGOCIO DEL CLIENTE - FACTURABLE||2|||Prueba2|
		|GBCO229 |H. Proyecto|P. generales CVDS ágiles|CAPACITACIÓN RECIBIDA EN PROCESO DE NEGOCIO DEL CLIENTE - FACTURABLE||2|||Prueba3|
		Entonces ella visualiza Reporte de tiempos / Días pendientes por cerrar
