package com.choucair.formacion.steps;

import com.choucair.formacion.pageobjects.*;

import net.thucydides.core.annotations.Step;


public class PopupValidationSteps {

	ColorlibLoginPage colorlibLoginPage;
	ColorlibMenuPage colorlibMenuPage;
	
	
	
	//Autenticacion en colorlib
	@Step
		public void login_colorlib(String strUsuario, String strPass) {
		
		// Abrir navegador con la Url de Prueba
		colorlibLoginPage.open();
		
		//Ingresar Usuario demo
		//Ingresar Contraseña demo
		//Click en el botón Sign In
		
		colorlibLoginPage.IngresarDatos(strUsuario,strPass);
		
		//Verificar la Autenticación (label en home)
		colorlibLoginPage.VerificaHome();
		
		
	}
	
	//Ingresar a Funcionalidad Popup Validation
	@Step
	public void ingresar_form_validation(){
		colorlibMenuPage.menuFormValidation();
		
		
	}
	
	
	
	
	
	
	
}
