package co.com.proyectobase.screenplay.stepdefinitions;



import java.util.List;

import org.hamcrest.Matchers;
import org.openqa.selenium.WebDriver;

import co.com.proyectobase.screenplay.model.WebAutomation;
import co.com.proyectobase.screenplay.questions.LaRespuesta;
import co.com.proyectobase.screenplay.tasks.Diligenciar;
import co.com.proyectobase.screenplay.tasks.Ingresar;
import cucumber.api.java.Before;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.Managed;


public class AutomationDemoStepDefinition {

	@Managed(driver="chrome")
	private WebDriver hisBrowser;
	private Actor carlos = Actor.named("Carlos");

	@Before
	public void configuracionInicial(){
		
		carlos.can(BrowseTheWeb.with(hisBrowser));
	}


@Dado("^que Carlos quiere acceder a la Web Automation Demo Site$")
public void queCarlosQuiereAccederALaWebAutomationDemoSite() {
   
	carlos.wasAbleTo(Ingresar.paginaInicial());
}


@Cuando("^el realiza el registro en la página$")
public void elRealizaElRegistroEnLaPágina(List<WebAutomation> dtDatosForm) {

		
		carlos.attemptsTo(Diligenciar.formularioAutomation(dtDatosForm));
		
	
}

@Entonces("^el verifica que se carga la pantalla con texto (.*)$")
public void elVerificaQueSeCargaLaPantallaConTextoDoubleClickOnEditIconToEDITTheTableRow(String palabra)  {
   carlos.should(GivenWhenThen.seeThat(LaRespuesta.es(), Matchers.equalTo(palabra)));
}
	
	
}
