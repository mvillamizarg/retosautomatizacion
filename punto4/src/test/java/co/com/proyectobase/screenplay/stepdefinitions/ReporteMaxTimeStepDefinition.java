package co.com.proyectobase.screenplay.stepdefinitions;

import java.util.List;

import org.hamcrest.Matchers;
import org.openqa.selenium.WebDriver;

import co.com.proyectobase.screenplay.model.Actividad;
import co.com.proyectobase.screenplay.questions.LaRespuesta;
import co.com.proyectobase.screenplay.tasks.Ingresar;
import co.com.proyectobase.screenplay.tasks.Reportar;
import cucumber.api.java.Before;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.Managed;

public class ReporteMaxTimeStepDefinition {
	
	@Managed(driver = "chrome")
	private WebDriver hisBrowser;
	private Actor mayra = Actor.named("Mayra");
	
	@Before
	public void configuracionInicial(){
		
		mayra.can(BrowseTheWeb.with(hisBrowser));
		
	}
	
	@Dado("^que Mayra quiere reportar sus actividades en MaxTime con usuario \"([^\"]*)\" y contraseña \"([^\"]*)\"$")
	public void queMayraQuiereReportarSusActividadesEnMaxTimeConUsuarioYContraseña(String usuario, String password){
	    
		mayra.wasAbleTo(Ingresar.maxTime(usuario, password));
		
	}


	@Cuando("^ella realiza el registro de la tareas en la aplicación$")
	public void ellaRealizaElRegistroDeLaTareasEnLaAplicación(List<Actividad> actividad) throws Exception {
		
		
	    mayra.attemptsTo(Reportar.actividadMaxTime(actividad));
	}

	@Entonces("^ella visualiza (.*)$")
	public void ellaVisualizaReporteDeTiemposDíasPendientesPorCerrar(String mensaje) {
	   
		mayra.should(GivenWhenThen.seeThat(LaRespuesta.es(), Matchers.equalTo(mensaje)));
	}


	

	
}
