package co.com.proyectobase.screenplay.userinterface;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;


public class AdministrarDoctorPageObject extends PageObject {

	public static final Target NOMBRE_DOCTOR = Target.the("Campo Ingresar Nombre").located(By.id("name"));
	public static final Target APELLIDO_DOCTOR = Target.the("Campo Ingresar Apellido").located(By.id("last_name"));
	public static final Target TELEFONO_DOCTOR = Target.the("Campo Ingresar Telefono").located(By.id("telephone"));
	public static final Target TIPO_DOCUMENTO = Target.the("Campo Seleccionar Tipo Documento").located(By.id("identification_type"));
	public static final Target NUMERO_DOCUMENTO = Target.the("Campo Ingresar Número Identidad").located(By.id("identification"));
	public static final Target BOTON_GUARDAR = Target.the("Botón Guardar").located(By.xpath("//*[@id=\"page-wrapper\"]/div/div[3]/div/a"));
	public static final Target MENU_DOCTOR = Target.the("Menú registro Doctores").located(By.xpath("//*[@id=\"page-wrapper\"]/div/div[2]/div/div/div/div/div[1]/div/a[1]"));
	


	
	
}