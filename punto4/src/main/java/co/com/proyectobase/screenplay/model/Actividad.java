package co.com.proyectobase.screenplay.model;

public class Actividad {
	
	private String proyecto;
	private String tipoHora;
	private String servicio;
	private String actividad;
	private String causalOcioso;
	private String horasEjecutadas;
	private String horasAdicionalesP;
	private String horasAdicionalesC;
	private String comentario;
	
	
	public String getProyecto() {
		return proyecto;
	}
	public void setProyecto(String proyecto) {
		this.proyecto = proyecto;
	}
	
	public String getTipoHora() {
		return tipoHora;
	}
	public void setTipoHora(String tipoHora) {
		this.tipoHora = tipoHora;
	}
	
	public String getServicio() {
		return servicio;
	}
	public void setServicio(String servicio) {
		this.servicio = servicio;
	}
	
	public String getActividad() {
		return actividad;
	}
	public void setActividad(String actividad) {
		this.actividad = actividad;
	}
	
	public String getCausalOcioso() {
		return causalOcioso;
	}
	public void setCausalOcioso(String causalOcioso) {
		this.causalOcioso = causalOcioso;
	}
	
	public String getHorasEjecutadas() {
		return horasEjecutadas;
	}
	public void setHorasEjecutadas(String horasEjecutadas) {
		this.horasEjecutadas = horasEjecutadas;
	}
	
	
	public String getHorasAdicionalesP() {
		return horasAdicionalesP;
	}
	public void setHorasAdicionalesP(String horasAdicionalesP) {
		this.horasAdicionalesP = horasAdicionalesP;
	}
	
	
	public String getHorasAdicionalesC() {
		return horasAdicionalesC;
	}
	public void setHorasAdicionalesC(String horasAdicionalesC) {
		this.horasAdicionalesC = horasAdicionalesC;
	}
	
	public String getComentario() {
		return comentario;
	}
	public void setComentario(String comentario) {
		this.comentario = comentario;
	}

}
