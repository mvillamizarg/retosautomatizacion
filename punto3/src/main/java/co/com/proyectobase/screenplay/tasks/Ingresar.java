package co.com.proyectobase.screenplay.tasks;

import java.util.List;

import co.com.proyectobase.screenplay.userinterface.AdministrarPacientePageObject;
import cucumber.api.DataTable;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.SelectFromOptions;

public class Ingresar implements Task {

	private DataTable paciente;
	
		
	public Ingresar(DataTable paciente) {
		super();
		this.paciente = paciente;
	}

	@Override
	public <T extends Actor> void performAs(T actor) {
		
	actor.attemptsTo(Click.on(AdministrarPacientePageObject.MENU_PACIENTE));
		
	List<List<String>> data = paciente.raw();
	
	for(int i = 1; i<data.size(); i++){
		
		actor.attemptsTo(Enter.theValue(data.get(i).get(0).trim()).into(AdministrarPacientePageObject.NOMBRE_PACIENTE));
		actor.attemptsTo(Enter.theValue(data.get(i).get(1).trim()).into(AdministrarPacientePageObject.APELLIDO_PACIENTE));
		actor.attemptsTo(Enter.theValue(data.get(i).get(2).trim()).into(AdministrarPacientePageObject.TELEFONO_PACIENTE));
		actor.attemptsTo(SelectFromOptions.byVisibleText(data.get(i).get(3).trim()).from(AdministrarPacientePageObject.TIPO_DOCUMENTO));
		actor.attemptsTo(Enter.theValue(data.get(i).get(4).trim()).into(AdministrarPacientePageObject.DOCUMENTO));
		
		if(data.get(i).get(5).trim() == "Si")
		{
			actor.attemptsTo(Click.on(AdministrarPacientePageObject.CHECK_PREPAGADA));
		}
		
		actor.attemptsTo(Click.on(AdministrarPacientePageObject.BOTON_GUARDAR));
	}
		
		
	}

	public static Ingresar unPaciente(DataTable paciente) {
		
		return Tasks.instrumented(Ingresar.class, paciente);
	}

}
