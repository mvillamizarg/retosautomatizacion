package co.com.proyectobase.screenplay.tasks;

import java.util.List;
import co.com.proyectobase.screenplay.interactions.SeleccionarLista;
import co.com.proyectobase.screenplay.model.WebAutomation;
import static co.com.proyectobase.screenplay.userinterface.AutomationDemoPage.*;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.SelectFromOptions;


public class Diligenciar implements Task {
	


	private List<WebAutomation> dtDatosForm;

	
	

	public Diligenciar(List<WebAutomation> dtDatosForm) {
		super();
		this.dtDatosForm = dtDatosForm;
	
	}

	@Override
	public <T extends Actor> void performAs(T actor) {
	
		// Este es el deber ser
		

		WebAutomation web = dtDatosForm.get(0);
		actor.attemptsTo(Enter.theValue(web.getNombre()).into(NOMBRE)); 
		actor.attemptsTo(Enter.theValue(web.getApellido()).into(APELLIDO));
		actor.attemptsTo(Enter.theValue(web.getDireccion()).into(DIRECCION));
		actor.attemptsTo(Enter.theValue(web.getEmail()).into(EMAIL));
		actor.attemptsTo(Enter.theValue(web.getTelefono()).into(TELEFONO));
		
		if(web.getGenero()!=""){
			
			if(web.getGenero().equals("Femenino")){
				
				actor.attemptsTo(Click.on(GENERO_FEMENINO));
				
			}else{
				
				actor.attemptsTo(Click.on(GENERO_MASCULINO));
			}
			
		}
		
		actor.attemptsTo(Click.on(HOBBIES_CRICKET));
		actor.attemptsTo(Click.on(HOBBIES_MOVIES));
		actor.attemptsTo(Click.on(LENGUAJE));
		actor.attemptsTo(SeleccionarLista.Desde(LENGUAJE_LIST, web.getIdioma()));
		actor.attemptsTo(SeleccionarLista.Desde(LENGUAJE_LIST, web.getIdioma1()));
		actor.attemptsTo(SelectFromOptions.byVisibleText(web.getHabilidad()).from(HABILIDAD));
		actor.attemptsTo(SelectFromOptions.byVisibleText(web.getCiudad()).from(CIUDAD));
		actor.attemptsTo(Click.on(CIUDAD1));
		actor.attemptsTo(SeleccionarLista.Desde(CIUDAD1_LIST, web.getCiudad2()));
		actor.attemptsTo(SelectFromOptions.byVisibleText(web.getAno()).from(ANO));
		actor.attemptsTo(SelectFromOptions.byVisibleText(web.getMes()).from(MES));
		actor.attemptsTo(SelectFromOptions.byVisibleText(web.getDia()).from(DIA));
		actor.attemptsTo(Enter.theValue(web.getPassword1()).into(PASSWORD));
		actor.attemptsTo(Enter.theValue(web.getPassword2()).into(PASSWORD_2));
		actor.attemptsTo(Click.on(BOTON_ENVIAR));
		
		
	}
	

	public static Diligenciar formularioAutomation(List<WebAutomation> dtDatosForm) {
	
		return Tasks.instrumented(Diligenciar.class, dtDatosForm);
	}

	
	
}
