package co.com.proyectobase.screenplay.tasks;

import co.com.proyectobase.screenplay.userinterface.OrangeHrPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Open;

public class Ingresar implements Task {
	
	OrangeHrPage orageHrPage;

	@Override
	public <T extends Actor> void performAs(T actor) {
		
		actor.attemptsTo(Open.browserOn(orageHrPage));
		actor.attemptsTo(Click.on(OrangeHrPage.BOTON_ENVIAR));
		actor.attemptsTo(Click.on(OrangeHrPage.PIM));
		actor.attemptsTo(Click.on(OrangeHrPage.ADD_EMPLEADO));
		
	}

	public static Ingresar paginaOrageHr() {
		// TODO Auto-generated method stub
		return Tasks.instrumented(Ingresar.class);
	}

}
