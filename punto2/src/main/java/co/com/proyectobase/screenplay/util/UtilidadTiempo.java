package co.com.proyectobase.screenplay.util;

import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

import org.awaitility.Awaitility;

public class UtilidadTiempo {

	private static Boolean allMessagesProcessed = true;

    public static void esperar(int tiempo) {
            Awaitility.await().forever().pollInterval(tiempo, TimeUnit.SECONDS).until(condicionExitosa());
    }

    public static Callable<Boolean> condicionExitosa() {
            return new Callable<Boolean>() {
                    public Boolean call() throws Exception {
                            return allMessagesProcessed;

                    }
            };
    }

    private UtilidadTiempo() {

    }
	
}
