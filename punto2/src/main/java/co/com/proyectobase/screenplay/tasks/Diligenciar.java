package co.com.proyectobase.screenplay.tasks;

import java.util.List;


import co.com.proyectobase.screenplay.userinterface.RegistroEmpleadoPage;
import cucumber.api.DataTable;
import net.serenitybdd.screenplay.Actor;

import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;




public class Diligenciar implements Task {

	private DataTable dtForms;
	
	
	
	
	public Diligenciar(DataTable dtForms) {
		super();
		this.dtForms = dtForms;
	}

	@Override
	public <T extends Actor> void performAs(T actor) {
		
		
		
		List<List<String>> data = dtForms.raw();
		
		for(int i = 1; i<data.size(); i++){
			
			actor.attemptsTo(Click.on(RegistroEmpleadoPage.ADD_MODAL));
			actor.attemptsTo(Enter.theValue(data.get(i).get(0).trim()).into(RegistroEmpleadoPage.NOMBRE));
			actor.attemptsTo(Enter.theValue(data.get(i).get(1).trim()).into(RegistroEmpleadoPage.SEGUNDO_NOMBRE));
			actor.attemptsTo(Enter.theValue(data.get(i).get(2).trim()).into(RegistroEmpleadoPage.APELLIDO));
			actor.attemptsTo(Click.on(RegistroEmpleadoPage.LOCACION));
			actor.attemptsTo(Click.on("//span[contains(text(),'" +data.get(i).get(3)+ "')]"));
			actor.attemptsTo(Click.on(RegistroEmpleadoPage.DETALLE));
			actor.attemptsTo(Enter.theValue(data.get(i).get(14).trim()).into(RegistroEmpleadoPage.USUARIO));
			actor.attemptsTo(Enter.theValue(data.get(i).get(15).trim()).into(RegistroEmpleadoPage.PASSWORD));
			actor.attemptsTo(Enter.theValue(data.get(i).get(16).trim()).into(RegistroEmpleadoPage.PASSWORD1));
			actor.attemptsTo(Click.on(RegistroEmpleadoPage.ROL));
			actor.attemptsTo(Click.on("//span[contains(text(),'" +data.get(i).get(17)+ "')]"));
			actor.attemptsTo(Click.on(RegistroEmpleadoPage.BOTON));
			actor.attemptsTo(Click.on(RegistroEmpleadoPage.OTRO_ID));
			actor.attemptsTo(Enter.theValue(data.get(i).get(4)).into(RegistroEmpleadoPage.OTRO_ID));
			actor.attemptsTo(Enter.theValue(data.get(i).get(5)).into(RegistroEmpleadoPage.FECHA_DE_CUMPLEANO));
			actor.attemptsTo(Click.on(RegistroEmpleadoPage.ESTADO_CIVIL));
			actor.attemptsTo(Click.on("//span[contains(text(),'" +data.get(i).get(6)+ "')]"));
			
			if(data.get(i).get(7).trim().equals("Femenino")){
				
				actor.attemptsTo(Click.on(RegistroEmpleadoPage.GENERO_FEMENINO));
			}else{
				
				actor.attemptsTo(Click.on(RegistroEmpleadoPage.GENERO_MASCULINO));
			}
			
			actor.attemptsTo(Click.on(RegistroEmpleadoPage.NACIONALIDAD));
			actor.attemptsTo(Click.on("//span[contains(text(),'" +data.get(i).get(8)+ "')]"));
			actor.attemptsTo(Enter.theValue(data.get(i).get(9).trim()).into(RegistroEmpleadoPage.NUMERO_DE_LICENCIA));
			actor.attemptsTo(Enter.theValue(data.get(i).get(10).trim()).into(RegistroEmpleadoPage.FECHA_EXPIRACION));
			actor.attemptsTo(Enter.theValue(data.get(i).get(11).trim()).into(RegistroEmpleadoPage.APODO));
			actor.attemptsTo(Click.on(RegistroEmpleadoPage.BOTON_DATOS_PERSONALES));
			
			actor.attemptsTo(Click.on(RegistroEmpleadoPage.GRUPO_SANGUINEO));
			actor.attemptsTo(Click.on("//span[contains(text(),'" +data.get(i).get(12)+ "')]"));
			actor.attemptsTo(Enter.theValue(data.get(i).get(13).trim()).into(RegistroEmpleadoPage.HOBBIES));
			actor.attemptsTo(Click.on(RegistroEmpleadoPage.BOTON_FINAL));
			

		}
		
			
	}

	public static Diligenciar formularioEmpleado(DataTable dtForms) {
		
		return Tasks.instrumented(Diligenciar.class, dtForms);
	}

}
