package co.com.proyectobase.screenplay.tasks;


import java.util.List;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import co.com.proyectobase.screenplay.interactions.SeleccionarLista;
import co.com.proyectobase.screenplay.model.Actividad;
import co.com.proyectobase.screenplay.util.UtilidadTiempo;
import static co.com.proyectobase.screenplay.userinterface.ReporteActividadPage.*;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.questions.Text;



public class Reportar implements Task {
	
	private List<Actividad> actividad;
	private static WebDriver driver = Serenity.getWebdriverManager().getCurrentDriver();  
	

	

	public Reportar(List<Actividad> actividad) {
		super();
		this.actividad = actividad;
	
	}

	@Override
	public <T extends Actor> void performAs(T actor) {
		
	UtilidadTiempo.esperar(8);	
    List<WebElement> listObjeto = CK_LABORAL.resolveFor(actor).findElements(By.tagName("tr"));
    
    for(int j=0; j < listObjeto.size(); j++){

        if (listObjeto.get(j).getAttribute("outerHTML").contains("UncheckedDisabled")) {
                   	 	
              actor.attemptsTo(Click.on(CERRAR_DIA));
              UtilidadTiempo.esperar(7);
              driver.switchTo().frame(0);
              UtilidadTiempo.esperar(7);
              actor.attemptsTo(Click.on(CONFIRMAR_CERRAR_DIA));
              driver.switchTo().defaultContent();
                    
         }else { 
                   	
         	 actor.attemptsTo(Click.on(NUEVO_REGISTRO));
                    	
         	for(int i =0; i< actividad.size(); i++){

    			Actividad act = actividad.get(i);
    			UtilidadTiempo.esperar(7);
    			actor.attemptsTo(Click.on(BOTON_PROYECTO));		
    			UtilidadTiempo.esperar(7);
    	
    			driver.switchTo().frame(0);
    			actor.attemptsTo(Enter.theValue(act.getProyecto()).into(BUSCAR_PROYECTO));
    			actor.attemptsTo(Click.on(BOTON_BUSCAR));
    			UtilidadTiempo.esperar(7);
    			actor.attemptsTo(Click.on(SELECCIONAR_REGISTRO));
    			UtilidadTiempo.esperar(7);
    			driver.switchTo().defaultContent();
    			
    			actor.attemptsTo(Click.on(TIPO_HORA));
    			actor.attemptsTo(SeleccionarLista.Desde(SELECCIONAR_TIPO_HORA, act.getTipoHora()));
    			
    			actor.attemptsTo(Click.on(BOTON_SERVICIO));
    			UtilidadTiempo.esperar(7);
    			driver.switchTo().frame(0);
    			actor.attemptsTo(Enter.theValue(act.getServicio()).into(BUSCAR_SERVICIO));
    			UtilidadTiempo.esperar(7);
    			actor.attemptsTo(Click.on(BOTON_BUSCARS));
    			UtilidadTiempo.esperar(7);
    			actor.attemptsTo(Click.on("//span[contains(text(),'" +act.getServicio()+ "')]"));
    			driver.switchTo().defaultContent();
    			
    			
    			actor.attemptsTo(Click.on(ACTIVIDAD));
    			actor.attemptsTo(SeleccionarLista.Desde(SELECCIONAR_ACTIVIDAD, act.getActividad()));
    			actor.attemptsTo(Enter.theValue(act.getHorasEjecutadas()).into(HORAS_EJECUTADAS));
    			actor.attemptsTo(Enter.theValue(act.getComentario()).into(COMENTARIO));
    			
    			if(actividad.size() == i){
    				
    				actor.attemptsTo(Click.on(GUARDAR_CERRAR));
    				UtilidadTiempo.esperar(7);
    				
    				String horasLaborales = Text.of(HORAS_LABORALES).viewedBy(actor).asString();
    				String horasReportadas = Text.of(HORAS_REPORTADAS).viewedBy(actor).asString();
    				
    				if(horasLaborales.equals(horasReportadas)){
    					
        				UtilidadTiempo.esperar(7);
        				actor.attemptsTo(Click.on(CERRAR_DIA));
        	            UtilidadTiempo.esperar(7);
        	            driver.switchTo().frame(0);
        	            UtilidadTiempo.esperar(7);
        	            actor.attemptsTo(Click.on(CONFIRMAR_CERRAR_DIA));
        	            driver.switchTo().defaultContent();
    					
    				} else {
    					
    					break;
    				}
    			         	
    			}else{
    				
    				actor.attemptsTo(Click.on(GUARDAR_NUEVO));
    				
    			}
    			
    			
    		}
         	 
           }     
         }		

	}


	public static Performable actividadMaxTime(List<Actividad> actividad) {
		
		return Tasks.instrumented(Reportar.class, actividad);
	}

}
