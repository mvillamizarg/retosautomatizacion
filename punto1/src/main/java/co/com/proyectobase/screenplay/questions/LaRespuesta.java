package co.com.proyectobase.screenplay.questions;

import co.com.proyectobase.screenplay.userinterface.ListDemoPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

public class LaRespuesta implements Question<String> {

	@Override
	public String answeredBy(Actor actor) {
		// TODO Auto-generated method stub
		return Text.of(ListDemoPage.MENSAJE).viewedBy(actor).asString();
	}
	
	public static LaRespuesta  es() {
		// TODO Auto-generated method stub
		return new LaRespuesta();
	}


}
