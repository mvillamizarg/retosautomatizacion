package co.com.proyectobase.screenplay.stepdefinitions;

import org.hamcrest.Matchers;
import org.openqa.selenium.WebDriver;

import co.com.proyectobase.screenplay.questions.LaRespuesta;
import co.com.proyectobase.screenplay.tasks.Abrir;
import co.com.proyectobase.screenplay.tasks.Agendar;
import co.com.proyectobase.screenplay.tasks.Ingresar;
import co.com.proyectobase.screenplay.tasks.Registrar;
import cucumber.api.DataTable;
import cucumber.api.java.Before;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.Managed;

public class GestionarCitaStepDefinition {
	
	@Managed(driver="chrome")
	public WebDriver hisBrowser;
	private Actor carlos = Actor.named("Carlos");

	@Before
	public void configuracion(){
		
		carlos.can(BrowseTheWeb.with(hisBrowser));
	}
	
	@Dado("^que Carlos necesita registrar un nuevo doctor$")
	public void queCarlosNecesitaRegistrarUnNuevoDoctor()  {
		carlos.wasAbleTo(Abrir.administrarHospital());
	}


	@Cuando("^el realiza el registro del doctor en el aplicativo de Administración de Hospitales$")
	public void elRealizaElRegistroDelDoctorEnElAplicativoDeAdministraciónDeHospitales(DataTable doctor)  {
	 
		carlos.attemptsTo(Registrar.unDoctor(doctor));
	}

	@Entonces("^el verifica que se presente en pantalla el mensaje (.*) para el registro doctor$")
	public void elVerificaQueSePresenteEnPantallaElMensajeDatosGuardadosCorrectamenteParaElRegistroDoctor(String parametro) {
	 
		carlos.should(GivenWhenThen.seeThat(LaRespuesta.es(), Matchers.equalTo(parametro)));
	}

	@Dado("^que Carlos necesita registrar un nuevo paciente$")
	public void queCarlosNecesitaRegistrarUnNuevoPaciente()  {
	   
		carlos.wasAbleTo(Abrir.administrarHospital());
	}

	@Cuando("^el realiza el registro del paciente en el aplicativo de Administración de Hospitales$")
	public void elRealizaElRegistroDelPacienteEnElAplicativoDeAdministraciónDeHospitales(DataTable paciente)  {
	    carlos.attemptsTo(Ingresar.unPaciente(paciente));
	}

	@Entonces("^el verifica que se presente en pantalla el mensaje (.*) para el registro paciente$")
	public void elVerificaQueSePresenteEnPantallaElMensajeDatosGuardadosCorrectamenteParaElRegistroPaciente(String parametro) {
	    carlos.should(GivenWhenThen.seeThat(LaRespuesta.es(), Matchers.equalTo(parametro)));
	}

	@Dado("^que Carlos necesita asistir al medico$")
	public void queCarlosNecesitaAsistirAlMedico() {
	      carlos.wasAbleTo(Abrir.administrarHospital());
	}

	@Cuando("^el realiza el agendamiento de una Cita$")
	public void elRealizaElAgendamientoDeUnaCita(DataTable cita){
	  carlos.attemptsTo(Agendar.unaCita(cita));
		
	}

	@Entonces("^el verifica que se presente en pantalla el mensaje (.*) para el registro cita$")
	public void elVerificaQueSePresenteEnPantallaElMensajeDatosGuardadosCorrectamenteParaElRegistroCita(String parametro) {
	   carlos.should(GivenWhenThen.seeThat(LaRespuesta.es(), Matchers.equalTo(parametro)));
	}

	
}
	
