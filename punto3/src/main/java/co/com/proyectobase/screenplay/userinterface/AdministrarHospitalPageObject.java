package co.com.proyectobase.screenplay.userinterface;


import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("http://automatizacion.herokuapp.com/pperez/home")
public class AdministrarHospitalPageObject extends PageObject {
	
	
	public static final Target MENSAJE = Target.the("Mensaje Confirmación").located(By.xpath("//*[@id=\'page-wrapper\']/div/div[2]/div[2]/p"));
	

}
