package com.choucair.formacion.pageobjects;




import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

public class ColorlibMenuPage extends PageObject {

	//Menu Forms
	@FindBy(xpath="//*[@id=\'menu\']/li[6]/a")
	public WebElementFacade menuForms;
	
	
	//Menu Form General
	@FindBy(xpath="//*[@id=\'menu\']/li[6]/ul/li[1]/a")
	public WebElementFacade menuFormGeneral;
	
	//Menu Form Validation
	@FindBy(xpath="//*[@id=\'menu\']/li[6]/ul/li[2]/a")
	public WebElementFacade menuFormValidation;
	
	//Form validation - label informatico
	@FindBy(xpath="//*[@id=\'content\']/div/div/div[1]/div/div/header/h5")
	public WebElementFacade lblFormValidation;
	
	public void menuFormValidation(){
		menuForms.click();
		menuFormValidation.click();
		String strMensaje = lblFormValidation.getText();
		String labelM = "Popup Validation";
		assertThat(strMensaje, containsString(labelM));
		
		
	}
	
	


}
