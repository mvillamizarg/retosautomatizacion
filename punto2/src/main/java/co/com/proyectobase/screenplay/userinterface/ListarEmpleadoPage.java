package co.com.proyectobase.screenplay.userinterface;

import org.openqa.selenium.By;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;

public class ListarEmpleadoPage extends PageObject {
	
	

	
	public static final Target LIST_EMPLEADO = Target.the("Agregar empleado").located(By.id("menu_pim_viewEmployeeList"));
 	public static final Target BUSQUEDA = Target.the("Campo donde ingreso el nombre").located(By.id("employee_name_quick_filter_employee_list_value"));
 	public static final Target BUSQUEDA1 = Target.the("Campo donde ingreso el nombre").located(By.id("employee_name_quick_filter_employee_list_dropdown"));
 	public static final Target CODIGO_BUSQUEDA =Target.the("Campo donde recupero el ID").located(By.id("angucomplete-title-temp-id"));
 	public static final Target CODIGO_ENCONTRADO = Target.the("Campo que recupera codigo en la tabla").located(By.xpath("//*[@id=\'employeeListTable\']/tbody/tr/td[2]"));  	

 	
 	
}