package co.com.proyectobase.screenplay.userinterface;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;

public class ReporteActividadPage extends PageObject {

	public static final Target NUEVO_REGISTRO = Target.the("Botón Nuevo registro").located(By.id("Vertical_v3_MainLayoutView_xaf_l103_xaf_dviReporteDetallado_ToolBar_Menu_DXI0_"));
	public static final Target CK_LABORAL = Target.the("Check para verificar si el dia es laboral").located(By.id("Vertical_v3_MainLayoutView_xaf_l59_xaf_dviLaboral_View"));
	public static final Target CERRAR_DIA = Target.the("Botón para cerrar día").located(By.id("Vertical_TB_Menu_DXI1_"));
	public static final Target CONFIRMAR_CERRAR_DIA = Target.the("Confiramar cierre").located(By.id("Dialog_actionContainerHolder_Menu_DXI0_"));
	public static final Target BOTON_PROYECTO = Target.the("Botón para seleccionar proyecto").located(By.xpath("//*[@id=\'Vertical_v6_MainLayoutEdit_xaf_l128_xaf_dviProyecto_Edit\']/tbody/tr/td[2]")); 
	public static final Target BUSCAR_PROYECTO = Target.the("Ingresar Id codigo interno proyecto").located(By.name("Dialog$SearchActionContainer$Menu$ITCNT0$xaf_a0$Ed"));
	public static final Target BOTON_BUSCAR = Target.the("Botón buscar proyecto").located(By.id("Dialog_SearchActionContainer_Menu_ITCNT0_xaf_a0_B_B"));
	public static final Target SELECCIONAR_REGISTRO = Target.the("Seleccionar registro la tabla").located(By.id("Dialog_v7_LE_v8_cell0_4_xaf_CodigoCliente"));
	public static final Target TIPO_HORA = Target.the("Campo para seleccionar el tipo hora").located(By.id("Vertical_v6_MainLayoutEdit_xaf_l148_xaf_dviTipoHora_Edit_DD_I"));
	public static final Target SELECCIONAR_TIPO_HORA = Target.the("Lista de tipo de hora ").located(By.id("Vertical_v6_MainLayoutEdit_xaf_l148_xaf_dviTipoHora_Edit_DD_DDD_L_LBT"));
	public static final Target BOTON_SERVICIO = Target.the("Botón para seleccionar servicio").located(By.xpath("//*[@id=\'Vertical_v6_MainLayoutEdit_xaf_l153_xaf_dviServicio_Edit_Find\']/tbody/tr/td"));	
	public static final Target BUSCAR_SERVICIO = Target.the("Ingresar Tipo de servicio").located(By.name("Dialog$SearchActionContainer$Menu$ITCNT0$xaf_a0$Ed"));
	public static final Target BOTON_BUSCARS = Target.the("Botón buscar servicio").located(By.id("Dialog_SearchActionContainer_Menu_ITCNT0_xaf_a0_B_B"));
	public static final Target ACTIVIDAD = Target.the("Campo para seleccionar la actividad").located(By.id("Vertical_v6_MainLayoutEdit_xaf_l158_xaf_dviActividad_Edit_DD_I"));
	public static final Target SELECCIONAR_ACTIVIDAD = Target.the("Lista de tipo de Actividad").located(By.id("Vertical_v6_MainLayoutEdit_xaf_l158_xaf_dviActividad_Edit_DD_DDD_L"));
	public static final Target HORAS_EJECUTADAS = Target.the("Campo para ingresar horas ejecutadas").located(By.id("Vertical_v6_MainLayoutEdit_xaf_l182_xaf_dviHoras_Edit_I"));
	public static final Target COMENTARIO = Target.the("Campo para ingresar horas ejecutadas").located(By.id("Vertical_v6_MainLayoutEdit_xaf_l207_xaf_dviComentario_Edit_I"));
	public static final Target GUARDAR_CERRAR = Target.the("Botón para Guardar y Cerrar").located(By.id("Vertical_EditModeActions2_Menu_DXI1_T"));
	public static final Target GUARDAR_NUEVO = Target.the("Botón para Guardar y Cerrar").located(By.id("Vertical_EditModeActions2_Menu_DXI2_T"));
	public static final Target HORAS_REPORTADAS = Target.the("Horas reportadas").located(By.id("Vertical_v3_MainLayoutView_xaf_l98_xaf_dviTotalHorasReportadas_View"));
	public static final Target HORAS_LABORALES = Target.the("Horas laborales").located(By.id("Vertical_v3_MainLayoutView_xaf_l64_xaf_dviHorasLaborales_View"));
	
}

