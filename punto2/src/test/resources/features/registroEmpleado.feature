# language: es
#Author: your.email@your.domain.com

@CasoExitoso
Característica: Registro de empleado OrageHR
   Como usuario 
   Quiero ingresar a OrageHR
   Registrar un empleado en la aplicación
   Verificar el registro del empleado


  Escenario: Registro Exitoso de un Empleado en el OrageHR
    Dado que Juan necesita crear un empleado en el OrageHR
    Cuando el realiza el ingreso del registro en la aplicación
    
    |Primer Nombre|Segundo Nombre|Apellido      |Locacion                   |Otro_id|Dia_Cumpleaños|Estado_Civil|Genero  |Nacionalidad|Licencia|Fecha_Licencia|Nick_name|Grupo_Sanguineo|Hobbies|UserName    |Password  |Password1 |RolAdmin    |
		|Lucio        |Manuel        |Villamizar    |     Australian Regional HQ|1258   |1990-08-15    |Single      |Masculino|Colombian  |58174963|2016-12-15    |maavigu  |AB             |Correr |manuvillamiz|!Paz123456|!Paz123456|Global Admin|
    
    Entonces el visualiza el registro Correcto nuevo empleado en el aplicativo

