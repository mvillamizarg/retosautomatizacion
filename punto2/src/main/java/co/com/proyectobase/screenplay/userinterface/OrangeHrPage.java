package co.com.proyectobase.screenplay.userinterface;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("https://orangehrm-demo-6x.orangehrmlive.com/")
public class OrangeHrPage extends PageObject{

	public static final Target BOTON_ENVIAR = Target.the("Boton Enviar").located(By.id("btnLogin"));
	public static final Target PIM = Target.the("Clic menú Módulo de información Personal").located(By.id("menu_pim_viewPimModule"));
	public static final Target ADD_EMPLEADO = Target.the("Agregar empleado").located(By.id("menu_pim_addEmployee"));

	
		
}
