package co.com.proyectobase.screenplay.tasks;

import java.util.List;

import co.com.proyectobase.screenplay.userinterface.AdministrarDoctorPageObject;
import cucumber.api.DataTable;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.SelectFromOptions;

public class Registrar implements Task {
	
	private DataTable doctor;
	
	

	public Registrar(DataTable doctor) {
		super();
		this.doctor = doctor;
	}

	@Override
	public <T extends Actor> void performAs(T actor) {
		
		actor.attemptsTo(Click.on(AdministrarDoctorPageObject.MENU_DOCTOR));
		
		List<List<String>> data = doctor.raw();
		
		for(int i = 1; i<data.size(); i++ ){
		
		actor.attemptsTo(Enter.theValue(data.get(i).get(0).trim()).into(AdministrarDoctorPageObject.NOMBRE_DOCTOR));
		actor.attemptsTo(Enter.theValue(data.get(i).get(1).trim()).into(AdministrarDoctorPageObject.APELLIDO_DOCTOR));
		actor.attemptsTo(Enter.theValue(data.get(i).get(2).trim()).into(AdministrarDoctorPageObject.TELEFONO_DOCTOR));
		actor.attemptsTo(SelectFromOptions.byValue(data.get(i).get(3).trim()).from(AdministrarDoctorPageObject.TIPO_DOCUMENTO));
		actor.attemptsTo(Enter.theValue(data.get(i).get(4).trim()).into(AdministrarDoctorPageObject.NUMERO_DOCUMENTO));
		actor.attemptsTo(Click.on(AdministrarDoctorPageObject.BOTON_GUARDAR));
		}
	}

	public static Registrar unDoctor(DataTable doctor) {
		
		return Tasks.instrumented(Registrar.class, doctor);
	}

}
