package co.com.proyectobase.screenplay.tasks;

import java.util.List;

import co.com.proyectobase.screenplay.userinterface.AdministrarCitaPageObject;
import cucumber.api.DataTable;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

public class Agendar implements Task {
	
	private DataTable cita;
	
	

	public Agendar(DataTable cita) {
		super();
		this.cita = cita;
	}

	@Override
	public <T extends Actor> void performAs(T actor) {
		
		actor.attemptsTo(Click.on(AdministrarCitaPageObject.MENU_CITA));
		
		List<List<String>> data = cita.raw();
		
		for(int i = 1; i< data.size(); i++){
			
			actor.attemptsTo(Enter.theValue(data.get(i).get(0).trim()).into(AdministrarCitaPageObject.FECHA_CITA));
			actor.attemptsTo(Click.on(AdministrarCitaPageObject.DOCUMENTO_PACIENTE));
			actor.attemptsTo(Enter.theValue(data.get(i).get(1).trim()).into(AdministrarCitaPageObject.DOCUMENTO_PACIENTE));
			actor.attemptsTo(Enter.theValue(data.get(i).get(2).trim()).into(AdministrarCitaPageObject.DOCUMENTO_DOCTOR));
			actor.attemptsTo(Enter.theValue(data.get(i).get(3).trim()).into(AdministrarCitaPageObject.OBSERVACION));
			actor.attemptsTo(Click.on(AdministrarCitaPageObject.BOTON));
		}
		
	}

	public static Agendar unaCita(DataTable cita) {
		
		return Tasks.instrumented(Agendar.class, cita);
	}

}
