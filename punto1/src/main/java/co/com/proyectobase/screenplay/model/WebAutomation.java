package co.com.proyectobase.screenplay.model;

public class WebAutomation {
	
	private String nombre;
	private String apellido;
	private String direccion;
	private String email;
	private String telefono;
	private String genero;
	private String hobbies1;
	private String hobbies2;
	private String idioma;
	private String idioma1;
	private String habilidad;
	private String ciudad;
	private String ciudad2;
	private String ano; 
	private String mes;
	private String dia;
	private String password1;
	private String password2;
	
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getGenero() {
		return genero;
	}
	public void setGenero(String genero) {
		this.genero = genero;
	}
	public String getHobbies1() {
		return hobbies1;
	}
	public void setHobbies1(String hobbies1) {
		this.hobbies1 = hobbies1;
	}
	public String getHobbies2() {
		return hobbies2;
	}
	public void setHobbies2(String hobbies2) {
		this.hobbies2 = hobbies2;
	}
	public String getIdioma() {
		return idioma;
	}
	public void setIdioma(String idioma) {
		this.idioma = idioma;
	}
	public String getIdioma1() {
		return idioma1;
	}
	public void setIdioma1(String idioma1) {
		this.idioma1 = idioma1;
	}
	public String getHabilidad() {
		return habilidad;
	}
	public void setHabilidad(String habilidad) {
		this.habilidad = habilidad;
	}
	public String getCiudad() {
		return ciudad;
	}
	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}
	public String getCiudad2() {
		return ciudad2;
	}
	public void setCiudad2(String ciudad2) {
		this.ciudad2 = ciudad2;
	}
	public String getAno() {
		return ano;
	}
	public void setAno(String ano) {
		this.ano = ano;
	}
	public String getMes() {
		return mes;
	}
	public void setMes(String mes) {
		this.mes = mes;
	}
	public String getDia() {
		return dia;
	}
	public void setDia(String dia) {
		this.dia = dia;
	}
	public String getPassword1() {
		return password1;
	}
	public void setPassword1(String password1) {
		this.password1 = password1;
	}
	public String getPassword2() {
		return password2;
	}
	public void setPassword2(String password2) {
		this.password2 = password2;
	}
	
	
}
