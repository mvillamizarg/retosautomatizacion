package co.com.proyectobase.screenplay.stepdefinitions;



import org.hamcrest.Matchers;
import org.openqa.selenium.WebDriver;

import co.com.proyectobase.screenplay.questions.LaRespuesta;
import co.com.proyectobase.screenplay.tasks.Diligenciar;
import co.com.proyectobase.screenplay.tasks.Ingresar;
import cucumber.api.DataTable;
import cucumber.api.java.Before;
import cucumber.api.java.ast.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.Managed;

public class RegistroEmpleadoStepDefinition  {

	@Managed(driver="chrome")
	private WebDriver hisBrowser;
	private Actor juan = Actor.named("Juan");
	
	@Before
	public void configuracionInicial(){
		juan.can(BrowseTheWeb.with(hisBrowser));
		
	}
	
	@Dado("^que Juan necesita crear un empleado en el OrageHR$")
	public void queJuanNecesitaCrearUnEmpleadoEnElOrageHR() {
	    
		juan.wasAbleTo(Ingresar.paginaOrageHr());
	}


	@Cuando("^el realiza el ingreso del registro en la aplicación$")
	public void elRealizaElIngresoDelRegistroEnLaAplicación(DataTable dtForms)  {
		
		juan.attemptsTo(Diligenciar.formularioEmpleado(dtForms));
	}

	@Entonces("^el visualiza el registro (.*) nuevo empleado en el aplicativo$")
	public void elVisualizaElNuevoEmpleadoEnElAplicativo(String parametro) {
		
	juan.should(GivenWhenThen.seeThat(LaRespuesta.es(),Matchers.equalTo(parametro)));
		
	}
	
	
}
