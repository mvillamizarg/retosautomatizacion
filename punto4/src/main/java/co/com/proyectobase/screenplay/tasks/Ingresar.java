package co.com.proyectobase.screenplay.tasks;




import co.com.proyectobase.screenplay.interactions.SeleccionarLista;
import co.com.proyectobase.screenplay.userinterface.MaxTimePage;
import co.com.proyectobase.screenplay.util.UtilidadTiempo;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.Open;



public class Ingresar implements Task {
	
	private String usuario;
	private String password;
 
	
	
	MaxTimePage maxTimePage;

	public Ingresar(String usuario, String password) {
		super();
		this.usuario = usuario;
		this.password = password;
	}

	@Override
	public <T extends Actor> void performAs(T actor) {
		
		actor.attemptsTo(Open.browserOn(maxTimePage));
		actor.attemptsTo(Enter.theValue(usuario).into(MaxTimePage.USUARIO));
		actor.attemptsTo(Enter.theValue(password).into(MaxTimePage.PASSWORD));
		actor.attemptsTo(Click.on(MaxTimePage.BOTON));
		   	 
		
		UtilidadTiempo.esperar(7);
		
		actor.attemptsTo(Click.on(MaxTimePage.TABLA));
				
		actor.attemptsTo(Click.on(MaxTimePage.DESPLEGABLE));
		
		actor.attemptsTo(SeleccionarLista.Desde(MaxTimePage.LISTA_DESPLEGABLE, usuario));
			UtilidadTiempo.esperar(7);
			actor.attemptsTo(Click.on(MaxTimePage.DIA));
			
			
	}

	public static Performable maxTime(String usuario, String password) {
		
		return Tasks.instrumented(Ingresar.class, usuario, password);
	}

}
