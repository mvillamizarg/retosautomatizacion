package co.com.proyectobase.screenplay.questions;


import co.com.proyectobase.screenplay.userinterface.ListarEmpleadoPage;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.questions.Text;

public class LaRespuesta implements Question<String> {


	
	@Override
	public String answeredBy(Actor actor) {
		
		
		
		String respuesta, obtenido1="", obtenido;


		
		actor.attemptsTo(Click.on(ListarEmpleadoPage.LIST_EMPLEADO));
		actor.attemptsTo(Click.on(ListarEmpleadoPage.BUSQUEDA));
		actor.attemptsTo(Enter.theValue("Lucio Manuel Villamizar").into(ListarEmpleadoPage.BUSQUEDA));
		actor.attemptsTo(Click.on("//span[contains(text(),'" +"Lucio Manuel Villamizar"+ "')]"));
		obtenido = Text.of(ListarEmpleadoPage.CODIGO_BUSQUEDA).viewedBy(actor).asString();
		
		
		 try {
             Thread.sleep(5000);
             obtenido1 = Text.of(ListarEmpleadoPage.CODIGO_ENCONTRADO).viewedBy(actor).asString();
         } catch (InterruptedException e) {
             
             e.printStackTrace();
         }
		
		 
		if(obtenido.equals(obtenido1)){
			
			respuesta = "Correcto";
		}else {
			
			respuesta = "Incorrecto";
		}
		
		return respuesta;
	}

	public static LaRespuesta es() {
		
		return new LaRespuesta();
	}
	
	

}
