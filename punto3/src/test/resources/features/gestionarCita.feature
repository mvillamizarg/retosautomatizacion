#Author: your.email@your.domain.com
#language:es
@Regresion
Característica: Gestionar Cita Médica
  	Como paciente
		Quiero realizar la solicitud de una cita médica
		A través del sistema de Administración de Hospitales

	
	@Caso1
	Escenario: Realizar el Registro de un Doctor
		Dado que Carlos necesita registrar un nuevo doctor
		Cuando el realiza el registro del doctor en el aplicativo de Administración de Hospitales
		|Nombre Completo |Apellidos        |Telefono|Tipo de documento Identidad|Documento de identidad|
		|Julian Alejandro|Barbosa Gutierrez|6789871 |Pasaportes                 |1143346809            |
		
		Entonces el verifica que se presente en pantalla el mensaje Datos guardados correctamente. para el registro doctor
	
	Escenario: Realizar el Registro de un Paciente
		Dado que Carlos necesita registrar un nuevo paciente
		Cuando el realiza el registro del paciente en el aplicativo de Administración de Hospitales
		|Nombre Completo |Apellidos        |Telefono|Tipo de documento Identidad|Documento de identidad|Salud Prepagada|
		|Jose Alberto    |Beltran Castro   |6907080 |Cédula de ciudadanía       |73101365              |Si             |
		
		Entonces el verifica que se presente en pantalla el mensaje Datos guardados correctamente. para el registro paciente
	
	Escenario: Realizar el Agendamiento de una Cita
   Dado que Carlos necesita asistir al medico
   Cuando el realiza el agendamiento de una Cita
   |Día de la cita|Documento paciente|Documento doctor|Observaciones                                |
   |09/01/2018    |73101365          |1143346809      |El usuario debe presentar la historia clinica|
   Entonces el verifica que se presente en pantalla el mensaje Datos guardados correctamente. para el registro cita
	