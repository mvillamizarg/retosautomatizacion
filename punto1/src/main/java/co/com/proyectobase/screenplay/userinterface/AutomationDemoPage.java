package co.com.proyectobase.screenplay.userinterface;

import org.openqa.selenium.By;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("http://demo.automationtesting.in/Register.html")
public class AutomationDemoPage extends PageObject {
	
public static final Target NOMBRE = Target.the("Campo donde se ingresa los nombres").located(By.xpath("//*[@id=\'basicBootstrapForm\']/div[1]/div[1]/input"));
public static final Target APELLIDO = Target.the("Campo donde se ingresa el apellido").located(By.xpath("//*[@id=\'basicBootstrapForm\']/div[1]/div[2]/input"));
public static final Target DIRECCION = Target.the("Campo donde se ingresa la direccion").located(By.xpath("//*[@id=\'basicBootstrapForm\']/div[2]/div/textarea"));
public static final Target EMAIL = Target.the("Campo donde se ingresa el email").located(By.xpath("//*[@id=\'eid\']/input"));
public static final Target TELEFONO = Target.the("Campo donde se ingresa el telefono").located(By.xpath("//*[@id=\'basicBootstrapForm\']/div[4]/div/input"));
public static final Target GENERO_FEMENINO = Target.the("Campo donde se selecciona el genero femenino").located(By.xpath("//*[@id=\'basicBootstrapForm\']/div[5]/div/label[2]/input"));
public static final Target GENERO_MASCULINO = Target.the("Campo donde se selecciona el genero masculino").located(By.xpath("//*[@id=\'basicBootstrapForm\']/div[5]/div/label[1]/input"));
public static final Target HOBBIES_CRICKET = Target.the("Campo donde se selecciona el Hobbies cricket").located(By.id("checkbox1"));
public static final Target HOBBIES_MOVIES = Target.the("Campo donde se selecciona el Hobbies movies").located(By.id("checkbox2"));
public static final Target HOBBIES_HOCKEY = Target.the("Campo donde se selecciona el Hobbies hockey").located(By.id("checkbox3"));
public static final Target LENGUAJE = Target.the("Campo donde se selecciona el lenguaje").located(By.id("msdd"));
public static final Target LENGUAJE_LIST = Target.the("Lista de lenguajes").locatedBy("//*[@id=\'basicBootstrapForm\']/div[7]/div/multi-select/div[2]/ul");
public static final Target HABILIDAD = Target.the("Campo donde se selecciona ala habilidad").located(By.id("Skills"));
public static final Target CIUDAD = Target.the("Campo donde se selecciona ala Ciudad").located(By.id("countries"));
public static final Target CIUDAD1 = Target.the("Campo donde se selecciona ala Ciudad").located(By.xpath("//*[@id=\'basicBootstrapForm\']/div[10]/div/span/span[1]/span"));
public static final Target CIUDAD1_LIST = Target.the("Campo donde se selecciona ala Ciudad").located(By.id("select2-country-results"));
public static final Target ANO = Target.the("Campo donde se selecciona el año").located(By.id("yearbox"));
public static final Target MES = Target.the("Campo donde se selecciona el mes").located(By.xpath("//*[@id=\'basicBootstrapForm\']/div[11]/div[2]/select"));
public static final Target DIA = Target.the("Campo donde se selecciona el dias").located(By.id("daybox"));
public static final Target PASSWORD = Target.the("Campo para ingresar el password").located(By.id("firstpassword"));
public static final Target PASSWORD_2 = Target.the("Campo para confirmar el password").located(By.id("secondpassword"));
public static final Target BOTON_ENVIAR = Target.the("").located(By.id("submitbtn"));


}
