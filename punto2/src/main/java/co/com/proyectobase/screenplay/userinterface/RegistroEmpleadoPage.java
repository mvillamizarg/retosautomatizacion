package co.com.proyectobase.screenplay.userinterface;

import org.openqa.selenium.By;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;

public class RegistroEmpleadoPage extends PageObject {
	
	
	public static final Target ADD_MODAL = Target.the("Campo para ingresar el nombre del empleado").located(By.id("addEmployeeModal"));
	public static final Target NOMBRE = Target.the("Campo para ingresar el nombre del empleado").located(By.id("firstName"));
	public static final Target SEGUNDO_NOMBRE = Target.the("Campo para ingresar el nombre del empleado").located(By.id("middleName"));
	public static final Target APELLIDO = Target.the("Campo para ingresar el apellido del empleado").located(By.id("lastName"));
	public static final Target LOCACION = Target.the("Campo para seleccionar la locación").located(By.id("location_inputfileddiv"));
	public static final Target DETALLE = Target.the("Check para ingresar detalle").located(By.xpath("//*[@id=\'pimAddEmployeeForm\']/div[1]/div/materializecss-decorator[3]/div/sf-decorator/div/label"));
	public static final Target USUARIO = Target.the("Campo para ingresar nombre del usuario").located(By.id("username"));
	public static final Target PASSWORD = Target.the("Campo para ingresar el password").located(By.id("password"));
	public static final Target PASSWORD1 = Target.the("Campo para confirmar password").located(By.id("confirmPassword"));
	public static final Target ROL = Target.the("Campo para ingresar el Rol").located(By.id("adminRoleId_inputfileddiv"));
 	public static final Target BOTON = Target.the("BotonEnviar").located(By.id("systemUserSaveBtn"));
 	
 	public static final Target OTRO_ID = Target.the("Campo para ingresar Otro ID").located(By.id("other_id"));
 	public static final Target FECHA_DE_CUMPLEANO = Target.the("Campo para ingresar la fecha").located(By.id("date_of_birth"));
 	public static final Target ESTADO_CIVIL = Target.the("Selccionar esdtado civil").located(By.id("marital_status_inputfileddiv"));
 	public static final Target GENERO_FEMENINO = Target.the("Selección Sexo Femenino").located(By.xpath("//*[@id=\'pimPersonalDetailsForm\']/materializecss-decorator[3]/div/sf-decorator[3]/div/ul/li[2]/label")); 
 	public static final Target GENERO_MASCULINO = Target.the("Selección Sexo Masculino").located(By.xpath("//*[@id=\'pimPersonalDetailsForm\']/materializecss-decorator[3]/div/sf-decorator[3]/div/ul/li[1]/label")); 
 	public static final Target NACIONALIDAD = Target.the("Seleccionar Nacionalidad").located(By.id("nationality_inputfileddiv"));
 	public static final Target NUMERO_DE_LICENCIA = Target.the("Campo para ingresar número de licencia").located(By.id("driver_license"));
 	public static final Target FECHA_EXPIRACION = Target.the("Campo para ingresar la fecha de Expiración de la licencia").located(By.id("license_expiry_date"));
 	public static final Target APODO = Target.the("Campo para ingresar el apodo").located(By.id("nickName"));
 	public static final Target BOTON_DATOS_PERSONALES = Target.the("Boton para cargar los datos").located(By.id("pimPersonalDetailsForm"));
 	
 	public static final Target GRUPO_SANGUINEO = Target.the("Campo para seleccionar Grupo Sanguineo").located(By.id("1_inputfileddiv")); 
 	public static final Target HOBBIES = Target.the("Campo para ingresar el hobbies").located(By.id("5"));
 	public static final Target BOTON_FINAL = Target.the("Botón para almacenar los datos Important").located(By.id("content"));
 	public static final Target ID_ENCONTRADO = Target.the("Captura del id").located(By.id("employee_id"));

 	
 	
	
	
 	
 
 	
}