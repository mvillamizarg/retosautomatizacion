package co.com.proyectobase.screenplay.tasks;

import co.com.proyectobase.screenplay.userinterface.AdministrarHospitalPageObject;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Open;

public class Abrir implements Task {

	AdministrarHospitalPageObject administrarHospital;

	 
	
	@Override
	public <T extends Actor> void performAs(T actor) {
		
		
		actor.attemptsTo(Open.browserOn(administrarHospital));
		
		

	
	}
	
	

	public static Abrir administrarHospital() {
		
		return Tasks.instrumented(Abrir.class);
	}


}
